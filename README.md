# Lab5 -- Integration testing

## Homework

### BVA

| Parameter        | Possible classes              |
|------------------|-------------------------------|
| type             | budget, luxury, nonsense      |
| plan             | minute, fixed_price, nonsense |
| distance         | <=0, >0, >=1000001            |
| planned_distance | <=0, >0, >=1000001            |
| time             | <=0, >0, >=1000001            |
| planned_time     | <=0, >0, >=1000001            |
| inno_discount    | yes, no, nonsense             |

### Decision table

| type     | plan        | distance        | planned_distance | time            | planned_time    | discount | status  |
|----------|-------------|-----------------|------------------|-----------------|-----------------|----------|---------|
| nonsense | *           | *               | *                | *               | *               | *        | Invalid |
| *        | nonsense    | *               | *                | *               | *               | *        | Invalid |
| *        | *           | <=0             | *                | *               | *               | *        | Invalid |
| *        | *           | >=1000001       | *                | *               | *               | *        | Invalid |
| *        | *           | *               | <=0              | *               | *               | *        | Invalid |
| *        | *           | *               | >=1000001        | *               | *               | *        | Invalid |
| *        | *           | *               | *                | <=0             | *               | *        | Invalid |
| *        | *           | *               | *                | >=1000001       | *               | *        | Invalid |
| *        | *           | *               | *                | *               | <=0             | *        | Invalid |
| *        | *           | *               | *                | *               | >=1000001       | *        | Invalid |
| *        | *           | *               | *                | *               | *               | nonsense | Invalid |
| budget   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| budget   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| budget   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| budget   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| luxury   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| luxury   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| luxury   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| luxury   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |

## Solution

I wrote Python script which helps to run tests.

### Install dependencies

    pip install -r requirements.txt

### Get parameters

    python3 main.py params

This produces following output:

    Here is InnoCar Specs:
    Budet car price per minute = 16
    Luxury car price per minute = 48
    Fixed price per km = 13
    Allowed deviations in % = 7.000000000000001
    Inno discount in % = 15

These values will be used further in script.

### Tests

    python3 main.py tests

This command run tests and prints them as Markdown table, which is located below this line.

### Results

| type     | plan        | distance | planned_distance | time    | planned_time | inno_discount | expected        | actual          | passed |
|----------|-------------|----------|------------------|---------|--------------|---------------|-----------------|-----------------|--------|
| nonsense | minute      | 1        | 1                | 1       | 1            | yes           | Invalid Request | Invalid Request | Yes    |
| budget   | nonsense    | 1        | 1                | 1       | 1            | yes           | Invalid Request | Invalid Request | Yes    |
| budget   | minute      | -1       | 1                | 1       | 1            | yes           | Invalid Request | Invalid Request | Yes    |
| budget   | minute      | 1000001  | 1                | 1       | 1            | yes           | 13.6            | 11.57           | No     |
| budget   | minute      | 1        | -1               | 1       | 1            | yes           | Invalid Request | Invalid Request | Yes    |
| budget   | minute      | 1        | 1000001          | 1       | 1            | yes           | 13.6            | 11.57           | No     |
| budget   | minute      | 1        | 1                | -1      | 1            | yes           | Invalid Request | Invalid Request | Yes    |
| budget   | minute      | 1        | 1                | 1000001 | 1            | yes           | 13600013.6      | 11570011.57     | No     |
| budget   | minute      | 1        | 1                | 1       | -1           | yes           | Invalid Request | Invalid Request | Yes    |
| budget   | minute      | 1        | 1                | 1       | 1000001      | yes           | 13.6            | 11.57           | No     |
| budget   | minute      | 1        | 1                | 1       | 1            | nonsense      | Invalid Request | 13              | No     |
| budget   | minute      | 0        | 1                | 1       | 1            | yes           | Invalid Request | 11.57           | No     |
| budget   | minute      | 1000000  | 1                | 1       | 1            | yes           | 13.6            | 11.57           | No     |
| budget   | minute      | 1        | 0                | 1       | 1            | yes           | Invalid Request | 11.57           | No     |
| budget   | minute      | 1        | 1000000          | 1       | 1            | yes           | 13.6            | 11.57           | No     |
| budget   | minute      | 1        | 1                | 0       | 1            | yes           | Invalid Request | 0               | No     |
| budget   | minute      | 1        | 1                | 1000000 | 1            | yes           | 13600000.0      | 11570000        | No     |
| budget   | minute      | 1        | 1                | 1       | 0            | yes           | Invalid Request | 11.57           | No     |
| budget   | minute      | 1        | 1                | 1       | 1000000      | yes           | 13.6            | 11.57           | No     |
| budget   | minute      | 64       | 128              | 256     | 512          | yes           | 3481.6          | 2961.92         | No     |
| budget   | minute      | 64       | 128              | 256     | 512          | no            | 4096            | 3328            | No     |
| budget   | fixed_price | 64       | 128              | 256     | 512          | yes           | 3481.6          | 3797.3333333333 | No     |
| budget   | fixed_price | 64       | 128              | 256     | 512          | no            | 4096            | 4266.6666666666 | No     |
| luxury   | minute      | 64       | 128              | 256     | 512          | yes           | 10444.8         | 7655.4240000000 | No     |
| luxury   | minute      | 64       | 128              | 256     | 512          | no            | 12288           | 8601.6          | No     |
| luxury   | fixed_price | 64       | 128              | 256     | 512          | yes           | 10444.8         | Invalid Request | No     |
| luxury   | fixed_price | 64       | 128              | 256     | 512          | no            | 12288           | Invalid Request | No     |

Passed: 6
Failed: 21
Total: 27

### Found bugs

1. Validation for some of invalid data does not work
2. Price calculation is broken
