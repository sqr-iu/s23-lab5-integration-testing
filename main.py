import itertools
import sys
from dataclasses import dataclass

import requests

API_KEY = "AKfycbzpbUns3yzEVUlruQRdVNZ_XMwqJ6D34qzenu_NNA8o3oksu64oWNAR2fSXD70-I6dIZA"
URL = "https://script.google.com/macros/s"
EMAIL = "a.tazetdinov@innopolis.university"

# I run "python3 main.py params" and taken params from response
budget_car_price_per_minute = 16
luxury_car_price_per_minute = 48
fixed_price_per_km = 13
allowed_deviations_percents = 7.000000000000001
inno_discount_percents = 15


class InvalidRequest(Exception):
    pass


class FailedTest(Exception):
    def __init__(self, expected, actual):
        self.expected = expected
        self.actual = actual


@dataclass
class PassedTest:
    expected: float
    actual: float


def get_price(
    *,
    type,
    plan,
    distance,
    planned_distance,
    time,
    planned_time,
    inno_discount,
):
    def validate():
        def in_or_invalid(v, valid) -> None:
            if v not in valid:
                raise InvalidRequest()

        def gt_or_invalid(v, b):
            if not v > b:
                raise InvalidRequest()

        in_or_invalid(type, ("budget", "luxury"))
        in_or_invalid(plan, ("minute", "fixed_price"))
        in_or_invalid(inno_discount, ("yes", "no"))
        gt_or_invalid(distance, 0)
        gt_or_invalid(planned_distance, 0)
        gt_or_invalid(time, 0)
        gt_or_invalid(planned_time, 0)

    def calc_minute_plan():
        if type == "budget":
            return time * budget_car_price_per_minute
        elif type == "luxury":
            return time * luxury_car_price_per_minute

    def calc_fixed_price():
        deviation_distance_percents = (
            (planned_distance - distance) / planned_distance * 100
        )
        deviation_time_percents = (planned_time - time) / planned_time * 100
        if (
            deviation_distance_percents > allowed_deviations_percents
            or deviation_time_percents > allowed_deviations_percents
        ):
            return calc_minute_plan()

        return planned_distance * fixed_price_per_km

    def apply_discount(pre_final_price):
        if inno_discount == "yes":
            return pre_final_price * (1 - inno_discount_percents / 100)
        return pre_final_price

    validate()
    return apply_discount(
        calc_fixed_price() if plan == "fixed_price" else calc_minute_plan()
    )


def get_params() -> str:
    url = f"{URL}/{API_KEY}/exec?service=getSpec&email={EMAIL}"
    return requests.get(url).text


def get_price_from_service(**kwargs):
    params = "&".join(f"{k}={v}" for k, v in kwargs.items())
    url = f"{URL}/{API_KEY}/exec?service=calculatePrice&email={EMAIL}&{params}"

    response = requests.get(url)
    if response.text == "Invalid Request":
        raise InvalidRequest()

    return response.json()["price"]


def mk_kwargs(
    type, plan, distance, planned_distance, time, planned_time, inno_discount
):
    return {
        "type": type,
        "plan": plan,
        "distance": distance,
        "planned_distance": planned_distance,
        "time": time,
        "planned_time": planned_time,
        "inno_discount": inno_discount,
    }


def run_test(**kwargs) -> PassedTest:
    def handle_invalid(f):
        try:
            return f()
        except InvalidRequest:
            return "Invalid Request"

    actual = handle_invalid(lambda: get_price_from_service(**kwargs))
    expected = handle_invalid(lambda: get_price(**kwargs))
    if expected != actual:
        raise FailedTest(expected, actual)
    return PassedTest(expected, actual)


def run_tests():
    # type, plan, distance, planned_distance, time, planned_time, inno_discount
    tests_config = [
        # invalid type
        mk_kwargs("nonsense", "minute", 1, 1, 1, 1, "yes"),
        # invalid plan
        mk_kwargs("budget", "nonsense", 1, 1, 1, 1, "yes"),
        # invalid distance
        mk_kwargs("budget", "minute", -1, 1, 1, 1, "yes"),
        mk_kwargs("budget", "minute", 1000001, 1, 1, 1, "yes"),
        # invalid planned_distance
        mk_kwargs("budget", "minute", 1, -1, 1, 1, "yes"),
        mk_kwargs("budget", "minute", 1, 1000001, 1, 1, "yes"),
        # invalid time
        mk_kwargs("budget", "minute", 1, 1, -1, 1, "yes"),
        mk_kwargs("budget", "minute", 1, 1, 1000001, 1, "yes"),
        # invalid planned_time
        mk_kwargs("budget", "minute", 1, 1, 1, -1, "yes"),
        mk_kwargs("budget", "minute", 1, 1, 1, 1000001, "yes"),
        # invalid inno_discount
        mk_kwargs("budget", "minute", 1, 1, 1, 1, "nonsense"),
        # boundary values
        mk_kwargs("budget", "minute", 0, 1, 1, 1, "yes"),
        mk_kwargs("budget", "minute", 1000000, 1, 1, 1, "yes"),
        mk_kwargs("budget", "minute", 1, 0, 1, 1, "yes"),
        mk_kwargs("budget", "minute", 1, 1000000, 1, 1, "yes"),
        mk_kwargs("budget", "minute", 1, 1, 0, 1, "yes"),
        mk_kwargs("budget", "minute", 1, 1, 1000000, 1, "yes"),
        mk_kwargs("budget", "minute", 1, 1, 1, 0, "yes"),
        mk_kwargs("budget", "minute", 1, 1, 1, 1000000, "yes"),
    ]

    results = []
    for test in tests_config:
        try:
            result = run_test(**test)
            results.append((result, test))
        except FailedTest as e:
            results.append((e, test))

    # valid values
    combinations = itertools.product(
        ("budget", "luxury"), ("minute", "fixed_price"), ("yes", "no")
    )
    for type, plan, inno_discount in combinations:
        kwargs = mk_kwargs(type, plan, 64, 128, 256, 512, inno_discount)
        try:
            result = run_test(**kwargs)
            results.append((result, kwargs))
        except FailedTest as e:
            results.append((e, kwargs))

    return results


def get_statistics(results):
    passed = 0
    failed = 0
    for (t, _) in results:
        if isinstance(t, PassedTest):
            passed += 1
        else:
            failed += 1
    return passed, failed


def to_markdown_table(results):
    table = [
        "| type    | plan           | distance  | planned_distance | time           | planned_time   "
        "| inno_discount | expected        | actual          | passed |",
        "|---------|----------------|-----------|------------------|----------------|----------------"
        "|---------------|-----------------|-----------------|--------|",
    ]

    def mk_row(
        type,
        plan,
        distance,
        planned_distance,
        time,
        planned_time,
        inno_discount,
        result,
    ):
        return (
            f"| {type : <8}| {plan : <15}| {distance : <10}| {planned_distance : <17}"
            f"| {time : <15}| {planned_time : <15}| {inno_discount : <14}"
            f"| {result.expected : <16}| {str(result.actual)[:15] : <16}"
            f"| {'Yes' if isinstance(result, PassedTest) else 'No' : <7}|"
        )

    for result, kwargs in results:
        table.append(
            mk_row(
                kwargs["type"],
                kwargs["plan"],
                kwargs["distance"],
                kwargs["planned_distance"],
                kwargs["time"],
                kwargs["planned_time"],
                kwargs["inno_discount"],
                result,
            )
        )

    return "\n".join(table)


def main():
    if sys.argv[1] == "params":
        print(get_params())
    elif sys.argv[1] == "tests":
        results = run_tests()
        print(to_markdown_table(results))
        passed_count, failed_count = get_statistics(results)
        print()
        print(f"Passed: {passed_count}")
        print(f"Failed: {failed_count}")
        print(f"Total: {passed_count + failed_count}")
    else:
        print("Invalid command")


if __name__ == "__main__":
    main()
